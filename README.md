# Twitter_granblue_delete

Interacts with the twitter api to delete granblue help tweets

## License
This work is licensed under [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/?ref=chooser-v1)
