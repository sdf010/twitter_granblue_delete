"""Deletes granblue help request tweets from twitter"""
import secrets
import hashlib
import base64
import random
import string
import requests
import yaml
from flask import Flask, request, redirect, session

app = Flask(__name__)


class TwitterAPI():
    def __init__(self, redirect_uri="https://localhost/", client_id=None, scope=[], token=None):
        if not client_id:
            raise TypeError("client ID is required")
        self.redirect_uri = redirect_uri
        self.client_id = client_id  # replace
        self.scope = scope  # replace
        self._session = None
        self._token = None
        if token:
            self.authenticate_as_user(token)

    def get_authorize_url(self):
        code_verifier = secrets.token_urlsafe(54)
        hashed = hashlib.sha256(code_verifier.encode('ascii')).digest()
        encoded = base64.urlsafe_b64encode(hashed)
        challenge = encoded.decode('ascii')[:-1]
        state = ''.join(random.choice(string.ascii_lowercase)
                        for i in range(10))

        url = "https://twitter.com/i/oauth2/authorize?response_type=code&"\
            "client_id={}&redirect_uri={}&scope={}&state={}&"\
            "code_challenge={}&code_challenge_method=S256".format(
              self.client_id, self.redirect_uri, '%20'.join(self.scope), state,
              challenge)
        return url, state, code_verifier

    def generate_access_token(self, code, code_verifier):
        auth_data = {'grant_type': 'authorization_code',
                     'code': code,
                     'client_id': self.client_id,
                     'redirect_uri': self.redirect_uri,
                     'code_verifier': code_verifier}
        response = requests.post(
            'https://api.twitter.com/2/oauth2/token', data=auth_data).json()

        token = response.get("access_token")
        self.authenticate_as_user(token)
        return response

    def is_authenticate_as_user(self):
        return bool(self._token)

    def authenticate_as_user(self, token):
        self._token = token
        self._session = requests.Session()
        self._session.headers.update({'Authorization': 'Bearer ' + token})

    def get_granblue_tweets(self):
        granblue_tweet_ids = []
        twitter_id = self.get_userid()
        url = "https://api.twitter.com/2/users/{}/tweets?max_results=100".format(
            twitter_id)
        response = self._session.get(url).json()
        for tweet in response['data']:
            if ':Battle ID\nI need backup!\nLvl ' in tweet['text']:
                granblue_tweet_ids.append(tweet)
        return granblue_tweet_ids

    def revoke_session(self):
        data = {
            "token": self._token,
            'client_id': self.client_id}
        self._token = self._session = None
        return requests.post(
            'https://api.twitter.com/2/oauth2/revoke', data=data).json()

    def get_userid(self):
        response = self._session.get(
            "https://api.twitter.com/2/users/me").json()
        return response['data']['id']

    def delete_tweet(self, tweet_id):
        url = "https://api.twitter.com/2/tweets/{}".format(tweet_id)
        self._session.delete(url)


def display_tweets(tweet_list):
    # I know I can use templates, being a bit lazy here
    page_html = """<html><body>"""
    for tweet in tweet_list:
        tweet_html = """
          <blockquote class="twitter-tweet">
          <p lang="en" dir="ltr">{}</p>&mdash; User goes here
          <a href="https://twitter.com/TwitterSupport/status/{}?ref_src=twsrc%5Etfw">Link</a>
          </blockquote>
          <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        """.format(tweet['text'], tweet['id'])
        page_html += tweet_html
    page_html += """
    <form method="post" id="confirmForm" action="/twgi">
    <label>Delete confirm:</label>
    <input type="submit" id="cancel" name="action" value="cancel"">
    <input type="submit" id="confirm" name="action" value="Confirm"">
    </form> """
    page_html += "</body></html>"
    return page_html


@app.route("/delete")
def twit_start():
    api = TwitterAPI(
        redirect_uri=app.config["redirect_uri"],
        scope=app.config["scope"],
        client_id=app.config["client_id"])

    url, state, code_verifier = api.get_authorize_url()
    session['state'], session['challenge'], = state, code_verifier
    return redirect(url, code=302)


@app.route("/twgi", methods=["GET", "POST"])
def twit_auth():
    if request.method == 'POST':
        if request.form['action'] == 'Confirm':
            api = TwitterAPI(
                redirect_uri=app.config["redirect_uri"],
                scope=app.config["scope"],
                client_id=app.config["client_id"],
                token=session['token'])
            for tweet in session['tweets']:
                api.delete_tweet(tweet['id'])
            api.revoke_session()
            return 'tweets deleted'
        api.revoke_session()
        return 'Deletion Canceled'

    code = request.args.get('code')
    state = request.args.get('state')
    if state != session['state']:
        return 'Something went wrong'
    api = TwitterAPI(
        redirect_uri=app.config["redirect_uri"],
        scope=app.config["scope"],
        client_id=app.config["client_id"])
    api.generate_access_token(code, session['challenge'])
    if not api.is_authenticate_as_user():
        return "Twitter auth failed"
    tweets = api.get_granblue_tweets()
    session['tweets'] = tweets
    session['token'] = api._token
    if not tweets:
        api.revoke_session()
        return "No tweets to delete"
    return display_tweets(tweets)


if __name__ == "__main__":
    with open("config.yaml", 'r') as config:
        data = yaml.safe_load(config)
        app.secret_key = data["app"]["secret_key"]
        app.config["redirect_uri"] = data["twitter_api"]["redirect_uri"]
        app.config["client_id"] = data["twitter_api"]["client_id"]
        app.config["scope"] = data["twitter_api"]["scope"]
    app.run(host="0.0.0.0", port=80)
